<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function get_products()
    {
        $products = Product::all();

        return response()->json($products);
    }

    public function add_product(Request $request)
    {
        $product = new Product();

        $product->name = $request->name;
        $product->price = $request->price;
        $product->photo = $request->photo;

        $product->save();
    }

    public function delete_product($id)
    {
        $product = Product::find($id);

        $product->delete();
    }

    public function getProduct($id)
    {
        $product = Product::find($id);

        return response()->json($product);
    }

    public function update_product(Request $request, $id)
    {
        $product = Product::find($id);

        $product->name = $request->name;
        $product->price = $request->price;
        $product->photo = $request->photo;

        $product->save();
    }
}
