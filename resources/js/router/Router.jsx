import React from "react";
import { Route, Routes } from "react-router-dom";
import Products from "../components/products";
import NewProduct from "../components/products/newProduct";
import NotFound from "../components/products/NotFound";
import EditProduct from "../components/products/editProduct";

const Router = () => {
    return (
        <div>
            <Routes>
                <Route path="/" element={<Products />} />
                <Route path="/product/new" element={<NewProduct />} />
                <Route path="/product/edit/:id" element={<EditProduct />} />
                <Route path="*" element={<NotFound />} />
            </Routes>
        </div>
    )
}

export default Router;
