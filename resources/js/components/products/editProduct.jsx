import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

const EditProduct = () => {
    const navigate = useNavigate();

    const { id } = useParams();

    const [product, setProduct] = useState([]);
    const [image, setImage] = useState('');
    const [file, setFile] = useState('');
    const [fileBase64, setFileBase64] = useState('');

    useEffect(() => {
        getProduct();
    }, []);

    const getProduct = async () => {
        await axios.get(`/api/get_product/${id}`).then(
            (response) => {
                setProduct(response.data);
                setImage(response.data.photo);
            }
        ).catch((err) => {
            console.log(err);
        });
    }

    const handleFrontImageChange = (e) => {
        setImage(URL.createObjectURL(e.target.files[0]));
        convertImageToBase64(e.target.files[0]);
    }

    const convertImageToBase64 = (file) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          setFileBase64(reader.result);
        };
        reader.onerror = (error) => {
          console.log("Error: ", error);
        };
    }

    const uploadToCloudinary = async (file, e) => {
        const formData = new FormData();
        formData.append("file", file);
        formData.append("upload_preset", "zncklcfx");
        return fetch("https://api.cloudinary.com/v1_1/palo23/image/upload", {
          method: "POST",
          body: formData,
        })
          .then((res) => res.json())
          .then((res) => {
            setProduct({
                ...product,
                photo: res.secure_url,
            })
            updateProduct(e);
          })
          .catch((err) => {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Error uploading image!',
              timer: 1500,
              showConfirmButton: false,
            });
            console.log(err)
          });
    }

    const handleUpdate = (e) => {
        e.preventDefault();

        if (product.name === '' || product.price === '') {
            toast.fire({
              icon: 'error',
              title: 'Please fill all fields!',
            });
        }

        if (fileBase64 === '') {
            updateProduct(e);
        } else {
            uploadToCloudinary(fileBase64, e);
        }
    }

    const updateProduct = async (e) => {
        e.preventDefault();
        const data = {
            name: product.name,
            price: product.price,
            photo: product.photo,
        };
        await axios.put(`/api/update_product/${id}`, data).then(
            (response) => {
                toast.fire({
                    icon: 'success',
                    title: 'Product updated successfully!',
                });
                navigate('/');
            }
        ).catch((err) => {
            toast.fire({
                icon: 'error',
                title: 'Error updating product!',
            });
        });
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    <div className="card p-4">
                        <div className="card-header">
                            <h4>Edit Product</h4>
                        </div>
                        <div className="card-body">
                            <form onSubmit={handleUpdate}>
                                <div className="form-group">
                                    <label htmlFor="product_name">Product Name</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="product_name"
                                        value={product.name}
                                        onChange={(e) => setProduct({ ...product, name: e.target.value })}
                                    />
                                </div>
                                <div className="form-group mt-3">
                                    <label htmlFor="product_price">Product Price</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="product_price"
                                        value={product.price}
                                        onChange={(e) =>setProduct({ ...product, price: e.target.value })}
                                    />
                                </div>
                                <div className="form-group mt-3">
                                    <label htmlFor="product_photo">Product Photo</label>
                                    {
                                        image && (
                                            <img src={image} alt="product" className="img-fluid" />
                                        )
                                    }
                                    <label htmlFor="front-upload" className="btn-upload mt-2">
                                        Add/Upload Image
                                    </label>
                                    <input
                                        type="file"
                                        className="form-control d-none"
                                        id="front-upload"
                                        onChange={handleFrontImageChange}
                                    />
                                </div>
                                <div className="form-group mt-3">
                                    <button type="submit" className="btn btn-primary">Update Product</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default EditProduct;
