import React, { useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";

const NewProduct = () => {
    const navigation = useNavigate();
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [image, setImage] = useState('');
    const [file, setFile] = useState('');
    const [fileBase64, setFileBase64] = useState('');

     const handleFrontImageChange = (e) => {
        const file = e.target.files[0];
        setFile(URL.createObjectURL(file))
        convertImageToBase64(file);
      }

      const convertImageToBase64 = (file) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          setFileBase64(reader.result);
        };
        reader.onerror = (error) => {
          console.log("Error: ", error);
        };
      }

      const uploadToCloudinary = async (file) => {
        const formData = new FormData();
        formData.append("file", file);
        formData.append("upload_preset", "zncklcfx");
        return fetch("https://api.cloudinary.com/v1_1/palo23/image/upload", {
          method: "POST",
          body: formData,
        })
          .then((res) => res.json())
          .then((res) => {
            setImage(res.secure_url);
            uploadProduct(res.secure_url);
          })
          .catch((err) => {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Error uploading image!',
              timer: 1500,
              showConfirmButton: false,
            });
            console.log(err)
          });
      }

      const handleSubmit = async (e) => {
        e.preventDefault();

        if (name === '' || price === '' || fileBase64 === '') {
            toast.fire({
              icon: 'error',
              title: 'Please fill all fields!',
            });
            return;
          }

        await uploadToCloudinary(fileBase64);
      }

        const uploadProduct = async (imageUrl) => {
            const data = {
                name: name,
                price: price,
                photo: imageUrl,
            };
            console.log(data);
            axios.post('/api/add_product', data)
                .then((res) => {
                    toast.fire({
                        icon: 'success',
                        title: 'Product added successfully!',
                    });
                    setName('');
                    setPrice('');
                    setImage('');
                    setFile('');
                    setFileBase64('');
                    navigation('/')
                })
                    .catch((err) => {
                toast.fire({
                    icon: 'error',
                    title: 'Error adding product!',
                });
            });
        }

    return (
        <div className="container p-5">

            <div className="card p-4">
            <div className="row">
                <div className="col-md-12">
                    <div className="form-group mt-3">
                        <label htmlFor="product_name">Product Name</label>
                        <input
                            type="text"
                            className="form-control"
                            id="product_name"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                        />
                    </div>
                </div>
                <div className="col-md-12 mt-3">
                    <div className="form-group">
                        <label htmlFor="product_price">Product Price</label>
                        <input
                            type="text"
                            className="form-control"
                            id="product_price"
                            value={price}
                            onChange={(e) => setPrice(e.target.value)}
                        />
                    </div>
                </div>
                <div className="col-md-12 mt-3">
                    <div className="form-group d-flex flex-column">
                        <label htmlFor="product_image">Product Image</label>
                        {
                            file && (
                                <img src={file} alt="product" className="img-fluid" />
                            )
                        }
                        <label htmlFor="front-upload" className="btn-upload mt-2">
                            Add/Upload Image
                        </label>
                        <input
                            type="file"
                            id="front-upload"
                            accept="image/*"
                            onChange={handleFrontImageChange}
                            class="d-none"
                        />
                    </div>
                </div>
            </div>

            <div className="row mt-5">
                <button
                    className="btn btn-block btn-primary"
                    onClick={handleSubmit}
                >
                    Save
                </button>
            </div>
            </div>


        </div>
    )
}

export default NewProduct;
