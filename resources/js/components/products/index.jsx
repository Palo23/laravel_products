import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

const Products = () => {

    const navigate = useNavigate();

    const [products, setProducts] = useState();

    const handleAddProduct = () => {
        navigate('/product/new');
    }

    useEffect(() => {
        getProducts();
    }, []);

    const getProducts = async () => {
        await axios.get('/api/get_products').then(
            (response) => {
                console.log(response.data);
                setProducts(response.data);
            }
        ).catch((err) => {
            console.log(err);
        });
    }

    const confirmDelete = (id) => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                deleteProduct(id);
            }
        }
        )
    }

    const deleteProduct = async (id) => {
        await axios.delete(`/api/delete_product/${id}`).then(
            (response) => {
                toast.fire({
                    icon: 'success',
                    title: 'Product deleted successfully!',
                });
                getProducts();
            }
        ).catch((err) => {
            toast.fire({
                icon: 'error',
                title: 'Error deleting product!',
            });
        });
    }

    const editProduct = (id) => {
        navigate(`/product/edit/${id}`);
    }

    return (
        <div className='container'>
            <div className="d-flex justify-content-between align-items-center mt-2 p-5">
                <h1>Products</h1>
                <button className='btn btn-block btn-primary' onClick={handleAddProduct}>
                    Add Product
                </button>
            </div>

            <div className="mt-2 d-flex justify-content-center align-items-center">
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Product Name</th>
                            <th>Product Price</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            products && products.map((product) => {
                                return (
                                    <tr>
                                        <td>
                                            <img src={product.photo} alt="product" className="img-fluid" />
                                        </td>
                                        <td>{product.name}</td>
                                        <td>{product.price}</td>
                                        <td>
                                            <button className='btn btn-block btn-primary mx-2 my-1' onClick={() => editProduct(product.id)}>
                                                Edit
                                            </button>
                                            <button className='btn btn-block btn-danger' onClick={() => confirmDelete(product.id)}>
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default Products;
