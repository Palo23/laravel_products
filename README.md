# Products Laravel

Example laravel app for jobbi test

## Installation

Use composerto install php packages

```bash
composer install
```

Use npm to install dependencies

```bash
npm install
```

Migrate database

```bash
php artisan migrate
```

## Usage

```bash
php artisan serve
```

```bash
npm run dev
```

## View project

[http://localhost:8000](http://localhost:8000)
